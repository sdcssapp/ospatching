#Steps
#------------------------------------
# 1. clone the bitbucket repo: git clone https://sdcssapp@bitbucket.org/sdcssapp/ospatching.git

# 2. cd ospatching

# 3. Pkg and upload required artifacts
aws cloudformation package --template-file master_cft.json --s3-bucket test-stage-bucket2 --s3-prefix lambda --output-template-file output.json

# 4. Run the cft
aws cloudformation deploy --template-file ./output.json --stack-name TestStack2

#5. Go to AWS Console --> AWS Systems Manager --> Documents --> Owned by me, Select the document and run 'Execute automation'.

# 6. To view already executed automations, go to AWS Console --> AWS Systems Manager --> Automation and click on the 'Execution ID'.



