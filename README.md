Please read cft_cli.sh for steps to be performed

The pem is sent in the email

The lambda function is under the 'lambda' folder. The script check for the AWS tag 'PatchTag'  
with value 'allow' to determine the instance-ids that need patching. 

Successful Execution with one instance:
https://console.aws.amazon.com/systems-manager/automation/execution/bc578692-a3bf-4b55-a7a5-d8f04d6b2a13?region=us-east-1

CloudWatch Logs for OS patching are at: CloudWatch --> Log Groups --> CloudWatchGroupForSSMAutomationService

Due to the issue mentioned in the python script, the test case for more than 1 instance is not available

Thank you
